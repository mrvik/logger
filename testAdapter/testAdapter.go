package testAdapter;

import(
    "testing"
  lgr "gitlab.com/mrvik/logger"
)

var _ lgr.Logger=&TestAdapterLogger{};

type TestAdapterLogger struct{
    testInterface testing.TB;
}

func CreateTestAdapter(tbinterface testing.TB) *TestAdapterLogger{
    return &TestAdapterLogger{tbinterface};
}

func (ta *TestAdapterLogger) IsDebug() bool{
    return true; //Always on debug
}

func (ta *TestAdapterLogger) Debug(str string){
    ta.testInterface.Log(str);
}

func (ta *TestAdapterLogger) Debugf(fmt string, args ...interface{}){
    ta.testInterface.Logf(fmt, args...);
}

func (ta *TestAdapterLogger) Info(str string){
    ta.testInterface.Log(str);
}

func (ta *TestAdapterLogger) Infof(fmt string, args ...interface{}){
    ta.testInterface.Logf(fmt, args...);
}

func (ta *TestAdapterLogger) Warn(str string){
    ta.testInterface.Log(str);
}

func (ta *TestAdapterLogger) Warnf(fmt string, args ...interface{}){
    ta.testInterface.Logf(fmt, args...);
}

func (ta *TestAdapterLogger) Error(str string){
    ta.testInterface.Error(str);
}

func (ta *TestAdapterLogger) Errorf(fmt string, args ...interface{}){
    ta.testInterface.Errorf(fmt, args...);
}
