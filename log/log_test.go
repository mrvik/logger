package log

import (
    "bytes"
    "testing"
)

func TestLogger(t *testing.T){
    var buf, errbuf bytes.Buffer;
    logger:=CreateDebug("", &buf, &errbuf);
    logger.Debug("Some string");
    if ln:=buf.Len(); ln<1{
        t.Errorf("Buffer has %d bytes after log. String has vanished\n", ln);
    }
    if ln:=errbuf.Len(); ln>0{
        t.Errorf("Unexpected bytes on error buf. Got %d\n", ln);
    }
    buf.Reset();
    errbuf.Reset();
    logger.Error("Some string which go to stderr");
    if ln:=buf.Len(); ln>0{
        t.Errorf("Unexpected bytes on stdbuf. Got %d\n", ln);
    }
    if ln:=errbuf.Len(); ln<1{
        t.Errorf("Error buffer has %d bytes after log. String has vanished\n", ln);
    }
}
