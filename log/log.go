//This package implements the logger interface in gitlab.com/mrvik/logger
//
//Output format is: (I|W|E) HEADER(specified on Create) TIMESTAMP MESSAGE
package log;

import(
    "fmt"
    "time"
    "os"
    "io"
  lgr "gitlab.com/mrvik/logger"
);

var _ lgr.Logger=new(Lgr); //This line will fail to compile if *Lgr doesn't implement Logger interface

//Struct containing the needed fileds for logging.
//Use the log.Create function to initialize it.
type Lgr struct{
    header string;
    output [2]io.Writer;
    debug bool;
}

//Create a new Logger. You can specify a header or leave it in blank.
//Also you can pass a io.Writter (e.g. a file writter) to stdout, stderr or both. If you leave it nil, will use the default stdout and stderr
func Create(header string, stdout, stderr io.Writer) *Lgr{
    return createLgr(header, stdout, stderr, false);
}

func CreateDebug(header string, stdout, stderr io.Writer) *Lgr{
    return createLgr(header, stdout, stderr, true);
}


func createLgr(header string, stdout, stderr io.Writer, debug bool) *Lgr{
    std, err:=stdout, stderr;
    if stdout==nil{
        std=os.Stdout;
    }
    if stderr==nil{
        err=os.Stderr;
    }
    l:=&Lgr{
        header:header,
        debug: debug,
        output: [2]io.Writer{std, err},
    };
    return l;
}

//Internal function. Handle the write to the writter specified on Logger.
func (l *Lgr) prn(out, addon string, writer int){
    fmt.Fprintf(l.output[writer-1], "(%s) %s %s %s", addon, l.header, l.getDate(), out);
}

//Internal function. Generate a date.
func (l *Lgr) getDate()string{
    dt:=time.Now();
    return dt.Format(time.Stamp);
}

//Will return true if logger has been created with CreateDebug. False otherwise
func (l *Lgr) IsDebug() bool{
    return l.debug;
}

//Log debug string if DEBUG is set to true
func (l *Lgr) Debug(out string){
    if l.debug {
        l.prn(out+"\n", "D", 1);
    }
}

//Log debug string if DEBUG is set to true
func (l *Lgr) Debugf(format string, args ...interface{}){
    if l.debug {
        l.prn(fmt.Sprintf(format, args...), "D", 1);
    }
}

//Log an info string.
func (l *Lgr) Info(out string){
    l.prn(out+"\n", "I", 1);
}

//Log an info string. Use this like fmt.Printf
func (l *Lgr) Infof(format string, args ...interface{}){
    l.prn(fmt.Sprintf(format, args...), "I", 1);
}

//Log a warning string. The warnings go to the stderr.
func (l *Lgr) Warn(out string){
    l.prn(out+"\n", "W", 2);
}

//Log a warning string. Warnings go to the stderr. Use this like fmt.Printf
func (l *Lgr) Warnf(format string, args ...interface{}){
    l.prn(fmt.Sprintf(format, args...), "W", 2);
}

//Log an error string.
func (l *Lgr) Error(out string){
    l.prn(out+"\n", "E", 2);
}

//Log an error string. Use this like fmt.Printf
func (l *Lgr) Errorf(format string, args ...interface{}){
    l.prn(fmt.Sprintf(format, args...), "E", 2);
}
