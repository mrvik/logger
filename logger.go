//Logger provides a simple log interface with custom prefixes (and date). Also can be piped to an external file
//This package can be imported on the current environment as it only contains a interface
package logger;

//This is a common interface that can be implemented by any package.
//See gitlab.com/mrvik/logger/log for implementation
type Logger interface{
    Debug(string);
    Debugf(string, ...interface{});
    Info(string);
    Infof(string, ...interface{});
    Warn(string);
    Warnf(string, ...interface{});
    Error(string);
    Errorf(string, ...interface{});
    IsDebug() bool;
}
