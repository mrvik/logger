package logdna

import (
    "bytes"
    "context"
    "encoding/json"
    "fmt"
    "net/http"
    "net/url"
    "os"
    "strconv"
    "time"
)

const (
    LOGDNA_ENDPOINT="http://logs.logdna.com/logs/ingest"
)

var (
    hostname=panicErr(os.Hostname())
    client=&http.Client{
        Timeout: time.Minute,
    }
)

func panicErr(str string, err error) string{
    if err!=nil{
        panic(err);
    }
    return str;
}

type dnaBody struct{
    Lines []dnaLine `json:"lines"`
}

func (dnal *DNALogger) sendLines(ctx context.Context, lines []dnaLine) (err error){
    if ctx.Err()!=nil{ //Fail fast if context was cancelled
        return;
    }
    stt:=dnaBody{
        Lines: lines,
    }
    body:=new(bytes.Buffer);
    encoder:=json.NewEncoder(body);
    err=encoder.Encode(stt);
    if err!=nil{
        return;
    }
    reqUrl, err:=url.ParseRequestURI(LOGDNA_ENDPOINT);
    if err!=nil{
        return;
    }
    query:=url.Values{
        "hostname": {
            hostname,
        },
        "now": {
            strconv.FormatInt(time.Now().Unix(), 10),
        },
    }
    reqUrl.RawQuery=query.Encode();
    req, err:=http.NewRequestWithContext(ctx, http.MethodPost, reqUrl.String(), body);
    if err!=nil{
        return;
    }
    req.Header.Set("Content-Type", "application/json; charset=UTF-8");
    req.SetBasicAuth(dnal.ingestionKey, "");
    res, err:=client.Do(req);
    if err!=nil{
        return;
    }
    if res.StatusCode<200||res.StatusCode>299{
        var body bytes.Buffer;
        body.ReadFrom(res.Body);
        err=fmt.Errorf("Bad response from LogDNA: %s\n", body.String());
    }
    return;
}
