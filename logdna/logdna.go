//This package sends logs to LogDNA and implements the Logger interface defined on parent package

package logdna

import (
    "context"
    "fmt"
    "gitlab.com/mrvik/logger"
    "runtime"
    "sync"
    "time"
)

type LogLevel string;

//Log levels (from https://docs.logdna.com/docs/ingestion#log-level)
const (
    DEBUG LogLevel="DEBUG"
    INFO="INFO"
    WARN="WARN"
    ERROR="ERROR"
)

type dnaLine struct{
    Line string `json:"line"`
    Timestamp int64 `json:"timestamp"`
    Level LogLevel `json:"level"`
    App string `json:"app"`
    Meta map[string]string `json:"meta"`
}

type DNALogger struct{
    debug, quiet bool
    logger logger.Logger
    ingestionKey, appName, logName string
    globalContext context.Context
    lines []dnaLine
    linesMutex sync.Mutex
    workerChan chan byte
}

var _ logger.Logger=&DNALogger{};

func WrapDNA(ctx context.Context, lgr logger.Logger, ingestionKey, appName, logName string) *DNALogger{
    var debug bool;
    if lgr!=nil{
        debug=lgr.IsDebug();
    }
    dnal:=&DNALogger{
        debug: debug,
        quiet: lgr==nil,
        logger: lgr,
        ingestionKey: ingestionKey,
        appName: appName,
        globalContext: ctx,
        logName: logName,
        workerChan: make(chan byte),
    }
    go dnal.workerThread(ctx);
    return dnal;
}

func (dnal *DNALogger) prn(level LogLevel, line string){
    dnaLine:=dnaLine{
        Line: line,
        Timestamp: time.Now().Unix(),
        Level: level,
        App: dnal.appName,
        Meta: map[string]string{
            "caller": dnal.getCaller(3),
            "logname": dnal.logName,
        },
    }
    dnal.linesMutex.Lock();
    defer dnal.linesMutex.Unlock();
    dnal.lines=append(dnal.lines, dnaLine);
    select{
    case dnal.workerChan<-0: //Notify of a changed line
    default: //Worker is not receiving messages. Give up
    }
}

func (*DNALogger) getCaller(num int) string{
    _, f,ln, _:=runtime.Caller(num);
    return fmt.Sprintf("%s#%d", f, ln);
}

func (dnal *DNALogger) workerThread(ctx context.Context){
    ticker:=time.NewTicker(time.Minute);
    loop:
    for{
        select{
        case <-ctx.Done():
            break loop;
        case _, ok:=<-dnal.workerChan:
            if !ok{
                break loop;
            }
        case <-ticker.C:
        }
        dnal.Flush();
    }
}

func (dnal *DNALogger) Flush(optionalContext ...context.Context){
    dnal.linesMutex.Lock();
    if len(dnal.lines)<1{
        dnal.linesMutex.Unlock();
        return;
    }
    ctx:=dnal.globalContext;
    if len(optionalContext)>0{
        ctx=optionalContext[0];
    }
    lines:=dnal.lines[:];
    dnal.lines=[]dnaLine{}; //Flush them and unlock the mutex to allow new lines to appear
    dnal.linesMutex.Unlock();
    err:=dnal.sendLines(ctx, lines);
    if err!=nil{
        dnal.linesMutex.Lock();
        dnal.lines=append(lines, dnal.lines...); //Put them back there
        dnal.linesMutex.Unlock();
        if dnal.logger!=nil{
            dnal.logger.Errorf("Error sending logs: %s\n", err);
        }
    }
}

func (dnal *DNALogger) Debug(str string){
    if !dnal.debug{
        return;
    }
    dnal.prn(DEBUG, str);
    if !dnal.quiet{
        dnal.logger.Debug(str);
    }
}

func (dnal *DNALogger) Debugf(format string, args ...interface{}){
    if !dnal.debug{
        return;
    }
    dnal.prn(DEBUG, fmt.Sprintf(format, args...));
    if !dnal.quiet{
        dnal.logger.Debugf(format, args...);
    }
}

func (dnal *DNALogger) Info(str string){
    dnal.prn(INFO, str);
    if !dnal.quiet{
        dnal.logger.Info(str);
    }
}

func (dnal *DNALogger) Infof(format string, args ...interface{}){
    dnal.prn(INFO, fmt.Sprintf(format, args...));
    if !dnal.quiet{
        dnal.logger.Infof(format, args...);
    }
}

func (dnal *DNALogger) Warn(str string){
    dnal.prn(WARN, str);
    if !dnal.quiet{
        dnal.logger.Warn(str);
    }
}

func (dnal *DNALogger) Warnf(format string, args ...interface{}){
    dnal.prn(WARN, fmt.Sprintf(format, args...));
    if !dnal.quiet{
        dnal.logger.Warnf(format, args...);
    }
}

func (dnal *DNALogger) Error(str string){
    dnal.prn(ERROR, str);
    if !dnal.quiet{
        dnal.logger.Error(str);
    }
}

func (dnal *DNALogger) Errorf(format string, args ...interface{}){
    dnal.prn(ERROR, fmt.Sprintf(format, args...));
    if !dnal.quiet{
        dnal.logger.Errorf(format, args...);
    }
}

func (dnal *DNALogger) IsDebug() bool{
    return dnal.debug;
}
